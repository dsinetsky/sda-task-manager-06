package ru.t1.dsinetsky.tm;

import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        if (argRun(args)) {
            exitApp();
            return;
        }
        displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nEnter command:");
            String command = scanner.nextLine();
            terminalRun(command);
        }

    }

    private static void displayHelp() {
        System.out.printf("%s,%s - shows this message;\n",ArgumentConst.CMD_HELP,TerminalConst.CMD_HELP);
        System.out.printf("%s,%s - shows program version;\n",ArgumentConst.CMD_VERSION,TerminalConst.CMD_VERSION);
        System.out.printf("%s,%s - shows information about developer;\n",ArgumentConst.CMD_ABOUT,TerminalConst.CMD_ABOUT);
        System.out.printf("%s - exit application.\n",TerminalConst.CMD_EXIT);
    }

    private static void displayVersion() {
        System.out.println("Version: 1.6.0");
    }

    private static void displayAbout() {
        System.out.println("Developer: Sinetsky Dmitry");
        System.out.println("Dev Email: dsinetsky@t1-consulting.ru");
    }

    private static void displayWelcome() {
        System.out.println("Welcome to the task-manager_02!");
        System.out.println("Type \"help\" for list of commands");
    }

    private static void displayError() {
        System.out.println("Invalid input! Type \"help\" or use argument \"-h\" for list of inputs");
    }

    private static void exitApp(){
        System.exit(0);
    }

    private static void terminalRun(final String command) {
        if (command == null) {
            displayError();
            return;
        }
        switch (command) {
            case (TerminalConst.CMD_HELP):
                displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                displayAbout();
                break;
            case (TerminalConst.CMD_EXIT):
                exitApp();
                break;
            default:
                displayError();
                break;
        }
    }

    private static void argumentRun(final String arg) {
        switch (arg) {
            case (ArgumentConst.CMD_HELP):
                displayHelp();
                break;
            case (ArgumentConst.CMD_VERSION):
                displayVersion();
                break;
            case (ArgumentConst.CMD_ABOUT):
                displayAbout();
                break;
            default:
                displayError();
                break;
        }
    }

    private static boolean argRun(final String[] args) {
        if (args.length < 1) {
            return false;
        }
        String param = args[0];
        argumentRun(param);
        return true;
    }
}

