package ru.t1.dsinetsky.tm.constant;

public final class ArgumentConst {

    public static final String CMD_HELP = "-h";

    public static final String CMD_ABOUT = "-a";

    public static final String CMD_VERSION = "-v";

}
