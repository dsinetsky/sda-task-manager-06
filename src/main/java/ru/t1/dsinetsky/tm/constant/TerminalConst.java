package ru.t1.dsinetsky.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_VERSION = "version";

    public static final String CMD_EXIT = "exit";

}
